# Define SSH key pair for  instances
#resource "aws_key_pair" "default" {
# key_name = "testpkf"
#  public_key = "${file("${var.key_path}")}"
#}
# Define server inside the public subnet
# Define server inside the public subnet
resource "aws_instance" "TESTAPP-Public-Server" {
  ami  = "${var.ami}"
  instance_type = "t2.micro"
  key_name = "testpkf"
  subnet_id = "${aws_subnet.public-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.testapp-public-sg.id}"]
  associate_public_ip_address = true
  source_dest_check = false

  tags {
    Name = "env-terraform-public-server"
  }
}
resource "aws_instance" "TESTAPP-VPN" {
  ami  = "${var.ami}"
  instance_type = "t2.micro"
  key_name = "vpnkey"
  subnet_id = "${aws_subnet.public-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.testapp-vpn-sg.id}"]
  associate_public_ip_address = true
  source_dest_check = false
  user_data = "${file("install.sh")}"

  tags {
    Name = "env-terraform-vpn-server"
  }
}

## Define server inside the private subnet
resource "aws_instance" "TESTAPP-Private-Server" {
  ami  = "${var.ami}"
  instance_type = "t2.micro"
  key_name = "TestAppKey"
  subnet_id = "${aws_subnet.private-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.testapp-private-sg.id}"]
  source_dest_check = false

  tags {
    Name = "env-terraform-private-server"
  }
}
