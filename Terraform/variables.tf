variable "aws_region" {
  description = "Region for the VPC"
  default = "us-east-2"
}

variable "vpc_cidr" {
  description = "CIDR for the VPC"
  default = "10.50.0.0/16"
}

variable "public_subnet_cidr" {
  description = "CIDR for the public subnet"
  default = "10.50.1.0/24"
}

variable "private_subnet_cidr" {
  description = "CIDR for the private subnet"
  default = "10.50.2.0/24"
}

variable "ami" {
  description = "Amazon Linux"
  default = "ami-0b59bfac6be064b78"
}

#variable "key_path" {
 # description = "SSH Public Key path"
  #default = "/home/core/.ssh/id_rsa.pub"

#variable "key_path" {
# description = "SSH Public Key path"
#  default = "C:/Users/vpulyaev/Downloads"
#}