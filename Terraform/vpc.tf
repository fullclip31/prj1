# Define our VPC
resource "aws_vpc" "testapp-vpc" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true

  tags {
    Name = "env-terraform-vpc"
  }
}
# Define the public subnet
resource "aws_subnet" "public-subnet" {
  vpc_id = "${aws_vpc.testapp-vpc.id}"
  cidr_block = "${var.public_subnet_cidr}"
  availability_zone = "us-east-2c"

  tags {
    Name = "env-terraform-public-subnet"
  }
}

# Define the private subnet
resource "aws_subnet" "private-subnet" {
  vpc_id = "${aws_vpc.testapp-vpc.id}"
  cidr_block = "${var.private_subnet_cidr}"
  availability_zone = "us-east-2c"

  tags {
    Name = "env-terraform-private-subnet"
  }
}

#  Define the internet gateway
resource "aws_internet_gateway" "testapp-igw" {
  vpc_id = "${aws_vpc.testapp-vpc.id}"

  tags {
    Name = "env-terraform-igw"
  }
}

# Define the route table
resource "aws_route_table" "public-rt" {
  vpc_id = "${aws_vpc.testapp-vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.testapp-igw.id}"
  }

  tags {
    Name = "env-terraform-public-rt"
  }
}

# Assign the route table to the public Subnet
resource "aws_route_table_association" "public-rt" {
  subnet_id = "${aws_subnet.public-subnet.id}"
  route_table_id = "${aws_route_table.public-rt.id}"
}

# Define the security group for public subnet
resource "aws_security_group" "testapp-public-sg" {
  name = "vpc_test_web"
  description = "Allow incoming HTTP connections & SSH access"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks =  ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id="${aws_vpc.testapp-vpc.id}"

  tags {
    Name = "env-terraform-public-sg"
  }
}

resource "aws_security_group" "testapp-vpn-sg" {
  name = "env-terraform-vpn-sg"
  description = "Allow incoming  VPN connections"


  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks =  ["0.0.0.0/0"]
  }

  ingress {
    from_port = 1194
    to_port = 1194
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id="${aws_vpc.testapp-vpc.id}"

  tags {
    Name = "env-terraform-vpn-sg"
  }
}
# Define the security group for private subnet
resource "aws_security_group" "testapp-private-sg"{
  name = "sg_test_web"
  description = "Allow traffic from public subnet"

  ingress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["${var.public_subnet_cidr}"]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["${var.public_subnet_cidr}"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${var.public_subnet_cidr}"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${aws_vpc.testapp-vpc.id}"

  tags {
    Name = "env-terraform-private-sg"
  }
}
# NAT gateway 4 Private Subnet
resource "aws_eip" "testapp-nat-eip" {
  vpc      = true
}
resource "aws_nat_gateway" "testapp-nat-gw" {
  allocation_id = "${aws_eip.testapp-nat-eip.id}"
  subnet_id = "${aws_subnet.public-subnet.id}"
  depends_on = ["aws_internet_gateway.testapp-igw"]
}

# NAT route table
resource "aws_route_table" "testapp-nat-rt" {
  vpc_id = "${aws_vpc.testapp-vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.testapp-nat-gw.id}"
  }

  tags {
    Name = "env-terraform-natrt"
  }
}
# NAT routes
resource "aws_route_table_association" "testapp-nat-route" {
  subnet_id = "${aws_subnet.private-subnet.id}"
  route_table_id = "${aws_route_table.testapp-nat-rt.id}"
}
