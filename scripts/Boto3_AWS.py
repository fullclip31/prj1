#!/usr/bin/env python
import boto3



# Создание VPC
vpc = ec2.create_vpc(CidrBlock='10.60.0.0/16')
vpc.create_tags(Tags=[{"Key": "Name", "Value": "default_vpc"}])
vpc.wait_until_available()
print(vpc.id)

# Создать и присоединить internet gateway
ig = ec2.create_internet_gateway()
vpc.attach_internet_gateway(InternetGatewayId=ig.id)
print(ig.id)

# Создать route table и public route
route_table = vpc.create_route_table()
route = route_table.create_route(
    DestinationCidrBlock='0.0.0.0/0',
    GatewayId=ig.id
)
print(route_table.id)

# Создать подстеть
subnet = ec2.create_subnet(CidrBlock='10.60.1.0/24', VpcId=vpc.id)
print(subnet.id)

# Присоединить route table к подсети
route_table.associate_with_subnet(SubnetId=subnet.id)

# Создать sec group
sec_group = ec2.create_security_group(
    GroupName='slice_0', Description='slice_0 sec group', VpcId=vpc.id)
sec_group.authorize_ingress(
    CidrIp='0.0.0.0/0',
    IpProtocol='icmp',
    FromPort=-1,
    ToPort=-1
)
print(sec_group.id)

# Image id ami-0f65671a86f061fcd / us-east-2
# Создать Инстанас (Amazon Linux)

instances = ec2.create_instances(
    ImageId='ami-0f65671a86f061fcd', InstanceType='t2.micro', MaxCount=1, MinCount=1,
    NetworkInterfaces=[{'SubnetId': subnet.id, 'DeviceIndex': 0, 'AssociatePublicIpAddress': True, 'Groups': [sec_group.group_id]}])
instances[0].wait_until_running()
print(instances[0].id)